#!/usr/bin/env python 

import time 
import sys
import shlex
import constants 
import util 
from expression import Expression 

class Database(object):  

    columns = {
        constants.C_ROWID:        [0, 9,  'int'],
        constants.C_PROJECT:      [1, 64, 'string'],
        constants.C_SHOT:         [2, 64, 'string'],
        constants.C_VERSION:      [3, 5,  'int'],
        constants.C_STATUS:       [4, 32, 'string'],
        constants.C_FINISH_DATE:  [5, 10, 'string'],
        constants.C_INTERNAL_BID: [6, 5,  'float'],
        constants.C_CREATED_DATE: [7, 16, 'string'],
        constants.C_IS_DELETED:   [8, 1,  'string'], 
    }
    
    db = []  # db table as loaded from db file 
    dbf = [] # db table after filter 
    
    filter_result_set = set()  
    
    def __init__(self):
        if constants.debug: print util.timestamp(), 'Class [' + self.__class__.__name__ + ']' 

    def generate_db_file(self, input_file): 
        util.logo()
        print('Loading from input file ' + input_file)

        # Read from input file
        # Earlier duplicated keys on PROJECT,SHOT,VERSION are discarded
        
        db = {} 
        counter = 0
        with open(input_file, 'r') as file: 
            file.readline() 
            for line in file: 
                if not line.strip() == '': 
                    util.log_nv(str(counter), line) 
                    items = line.split('|')
                    counter = counter + 1 
                    db[items[0].strip() + items[1].strip() + items[2].strip()] = \
                        items[0].strip() + '|' + \
                        items[1].strip() + '|' + \
                        items[2].strip() + '|' + \
                        items[3].strip() + '|' + \
                        items[4].strip() + '|' + \
                        items[5].strip() + '|' + \
                        items[6].strip() 
        
        # Write to db file 
        
        counter_o = 1 
        with open(constants.DB_FILE, 'w') as file: 
            for key, value in db.iteritems():
                file.write(str(counter_o) + '|' + value + '|N\n') 
                counter_o = counter_o + 1
            
            util.log_nv('input count', counter) 
            util.log_nv('output count', len(db))
            print('Saved db to ' + constants.DB_FILE)
            
        util.logc()
    
    def print_db(self): 
        util.log('') 
        if constants.debug: 
            for i in range(len(self.db)): 
                print(self.db[i])
            
    def print_dbf(self): 
        util.log('')
        if constants.debug: 
            for i in range(len(self.dbf)): 
                print(self.dbf[i])
            
    def load(self): 
        ''' Load db file into memory '''
        
        util.logo()
        with open(constants.DB_FILE, 'r') as file: 
            for line in file: 
                items = line.rstrip().split('|') 
                util.log(items[0]) 
                self.db.append(items)
        util.logc()
        
    def write_sql_file(self, file_name):
        ''' Export db as SQL insert statements to be used in query testing. '''
        
        self.load()
        with open(file_name,'w+') as file:
            for line in self.db:
                sql = 'insert into baseline values ('
                for i in range(len(line)):
                    if i in [0, 3, 6]:
                        sql = sql + line[i] + ', '
                    else: 
                        sql = sql + '"' + line[i] + '", '
                sql = sql[:-2] + '); \n' 
                file.write(sql)
        print('Database sql inserts written to ' + file_name)
            
    def write_csv_file(self, file_name): 
        ''' Export db as a flat csv file to be used in query testing. ''' 
        
        self.load()
        with open(file_name,'w+') as file:
            for line in self.db:
                csv = ''
                for i in range(len(line)):
                    csv = csv + '"' + line[i] + '",'
                csv = csv[:-2] + '"\n' 
                file.write(csv)
        print('Database csv written to ' + file_name)

    def find_matching_rows(self, condition): 
        ''' Return a set of matching row ids given a query condition. '''
        
        util.logo() 
        rowid_set = set()
        
        op = ''
        if '>=' in condition:
            op = '>='
        elif '<=' in condition:
            op = '<='
        elif '<>' in condition:
            op = '<>'
        elif '>' in condition:
            op = '>'
        elif '<' in condition:
            op = '<'
        elif '=' in condition:
            op = '='

        name, value = condition.split(op)
        if op == '=': op = '=='
        name = name.strip()
        value = value.strip() 
        column_index = self.columns[name][0]
        
        for i in range(len(self.db)): 
            if column_index in [self.columns[constants.C_VERSION][0], self.columns[constants.C_INTERNAL_BID][0]]:
                ex = self.db[i][column_index]+ op + value
            else:
                ex = '"' + self.db[i][column_index] + '"' + op + '"' + value + '"'
            util.log_nv(str(i), ex) 
            if eval(ex): 
                rowid_set.add(i + 1)   
                util.log_nv(str(i + 1), rowid_set) 
            
        util.log(rowid_set)
        util.logc()
        return rowid_set
        
    def filter(self, condition): 
        ''' Reduce db array as defined by query -f conditions. ''' 
        
        util.logo()
        util.log(condition)
        
        # Convert condition from infix to postfix
        
        ex = Expression() 
        operands = []
        util.log_nv('condition', condition) 
        tokens = shlex.split(ex.infix_to_postfix(condition))
        util.log_nv('tokens', tokens) 
        
        # Evaluate postfix condition 
        
        if ' AND ' in condition or ' OR ' in condition:
            for token in tokens:
                if ex.contains_operator(token):
                    operands.append([token, self.find_matching_rows(token)])
                else: 
                    operand2 = operands.pop()
                    operand1 = operands.pop()
                    
                    venn_result = {} 
                    if token == 'AND': 
                        venn_result = operand1[1].intersection(operand2[1])
                    elif token == 'OR':
                        venn_result = operand1[1].union(operand2[1])
                        
                    util.log('(' + operand1[0] + ' ' + token + ' ' + operand2[0] + ')')
                    result = '(' + operand1[0] + ' ' + token + ' ' + operand2[0] + ')'
                    operands.append([result, venn_result])
        else: 
            operands.append([tokens[0], self.find_matching_rows(tokens[0])])

        rtn = operands.pop()
        util.log(rtn)
        self.filter_result_set = rtn[1]
        
        self.dbf = []
        for row in self.db: 
            if int(row[self.columns[constants.C_ROWID][0]]) in self.filter_result_set: 
                self.dbf.append(list(row)) 
                
        util.logc()
        print

    def order(self, condition): 
        ''' Sort db as defined in -o or -g conditions. '''
        
        util.logo() 
        util.log(condition)
        self.print_db() 
        columns = condition.strip().split(',')
        for i in range(len(self.dbf)):
            sort_index = ''
            for column in columns: 
                column = column.strip() 
                if column == constants.C_VERSION: 
                    sort_index = sort_index + (self.dbf[i][self.columns[column][0]]).rjust(5, '0') 
                elif column == constants.C_INTERNAL_BID: 
                    sort_index = sort_index + (str("%.2f" % float(self.dbf[i][self.columns[column][0]]))).rjust(8, '0') 
                else: 
                    sort_index = sort_index + (self.dbf[i][self.columns[column][0]]).ljust(self.columns[column][1], '.')
            if len(self.dbf[i]) in [10, 11]: 
                self.dbf[i][9] = sort_index
            else:     
                self.dbf[i].append(sort_index)
        self.print_dbf() 
        self.dbf.sort(key = lambda x:x[9])     
        self.print_dbf() 
        util.logc()
    
    def select(self, condition=''):
        ''' Display final query result '''
        
        if condition == '': condition = constants.C_ALL_COLUMNS
        print 
        print(condition)
        
        columns = condition.strip().split(',')
        for i in range(len(self.dbf)):
            for column in columns: 
                column = column.strip() 
                if ':' in column:  
                    column = column.split(':')[0]
                print str(self.dbf[i][self.columns[column][0]]) + ('' if condition.endswith(column) else ','), 
            print 
    
    def group(self, select_condition, group_condition): 
        ''' Serve -g queries ''' 
        
        util.logo() 
        util.log(select_condition)
        util.log(group_condition)
        
        if len(self.dbf) == 0:
            util.log('No rows')
            return
        
        # Sort the db by group (-g) conditions 
        
        self.order(group_condition) 
        self.print_db() 
        self.print_dbf() 
        
        # Find start and end rows in each group 
        
        group_id = self.dbf[0][9]
        group_start = 0 
        grup_end = 0
        for i in range(len(self.dbf)):
            if group_id == self.dbf[i][9]:
                group_end = i
            else:
                self.dbf[group_start].append([group_start, group_end]) 
                group_id = self.dbf[i][9]
                group_start = i
                group_end = i
        self.dbf[group_start].append([group_start, group_end])
        self.print_dbf()
        
        # For each group compute aggregate functions as defined in -s 
        
        selects = select_condition.strip().split(',')
        
        for i in range(len(self.dbf)):
            if len(self.dbf[i]) == 11:
                util.log(self.dbf[i][9] + ' start *****')
                group_start = self.dbf[i][10][0]
                group_end = self.dbf[i][10][1]
                
                for select in selects:
                    if ':' in select:
                        column, aggregate = select.split(':')
                        
                        if aggregate == constants.GROUP_MIN: 
                            min = self.dbf[group_start][self.columns[column][0]]; 
                            for j in range(group_start + 1, group_end + 1):  
                                if float(self.dbf[j][self.columns[column][0]]) < float(min):
                                    min = self.dbf[j][self.columns[column][0]]
                            self.dbf[group_start][self.columns[column][0]] = min        
                        if aggregate == constants.GROUP_MAX: 
                            max = self.dbf[group_start][self.columns[column][0]]; 
                            for j in range(group_start + 1, group_end + 1):  
                                if float(self.dbf[j][self.columns[column][0]]) > float(max):
                                    max = self.dbf[j][self.columns[column][0]]
                            self.dbf[group_start][self.columns[column][0]] = max        
                        if aggregate == constants.GROUP_SUM: 
                            sum = 0
                            for j in range(group_start, group_end + 1):  
                                if self.columns[column][2] == 'float': 
                                    sum = sum + float(self.dbf[j][self.columns[column][0]]) 
                                elif self.columns[column][2] == 'int': 
                                    sum = sum + int(self.dbf[j][self.columns[column][0]]) 
                            self.dbf[group_start][self.columns[column][0]] = sum        
                        if aggregate == constants.GROUP_COUNT: 
                            self.dbf[group_start][self.columns[column][0]] = group_end - group_start + 1 
                        if aggregate == constants.GROUP_COLLECT: 
                            collect = {}
                            for j in range(group_start, group_end + 1):  
                                collect[self.dbf[j][self.columns[column][0]]] = ''
                            self.dbf[group_start][self.columns[column][0]] = list(collect.keys()) 
                            
        self.print_dbf()
        
        db_temp = []
        for row in self.dbf:
            if len(row) == 11:
                db_temp.append(list(row))
        self.dbf = list(db_temp)  
        self.print_dbf()

        util.logc()
        
if __name__ == "__main__":

    db = Database()

    if len(sys.argv) > 1: 
    
        args = util.parse_sys_argv(sys.argv, ['import', 'sql', 'csv'])
            
        if 'import' in args: 
            db.generate_db_file(args['import']) 
            
        elif 'sql' in args: 
            db.write_sql_file(args['sql']) 
        
        elif 'csv' in args: 
            db.write_csv_file(args['csv']) 
     
    else: 

        db.load() 
        
        db.filter('PROJECT="the hobbit" AND VERSION="99"')
        db.filter('(PROJECT="the hobbit" OR VERSION="55") OR STATUS="not required" AND PROJECT="king kong"')
        db.filter('(PROJECT<="the hobbit" OR VERSION="55" ) AND PROJECT="lotr"')
        db.filter('VERSION>="128"')    
        db.filter('PROJECT="the hobbit" OR PROJECT="lotr"')
        db.filter('FINISH_DATE<"2006-07-22" OR INTERNAL_BID>"30"')
        db.filter('VERSION>32 AND PROJECT<>"the hobbit"')    
        db.filter('VERSION>"32" OR PROJECT<>"the hobbit"')
        db.filter('STATUS<>"finished"')
        db.filter('INTERNAL_BID>23.50')
        db.filter('STATUS<>"finished" OR CREATED_DATE>"2006-10-15" AND STATUS<>"not required"')
        db.filter('STATUS<>"finished" OR CREATED_DATE="2010-03-22 01:10"')
        db.filter('PROJECT>0')
        
        db.order('PROJECT') 
        db.order('PROJECT,INTERNAL_BID,VERSION,CREATED_DATE') 
        db.order('PROJECT,SHOT, VERSION  ') 

        db.select('PROJECT,STATUS,INTERNAL_BID')
        db.select('PROJECT,STATUS,CREATED_DATE')
        db.select('PROJECT,STATUS:collect,VERSION:max')
        db.select()
        
        db.group('PROJECT,SHOT,INTERNAL_BID:sum,STATUS:collect ', 'PROJECT,SHOT')
        db.order('FINISH_DATE,CREATED_DATE')
        db.select('PROJECT,SHOT,INTERNAL_BID:sum,STATUS:collect ')
        
        db.filter('PROJECT="the hobbit"')
        