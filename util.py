#!/usr/bin/env python 

import datetime 
import sys
import constants 

def timestamp():
    return str(datetime.datetime.now().strftime("%y%m%d.%H%M%S")) 

def log(object = None, debug = constants.debug): 
    currentFuncName = lambda n=0: sys._getframe(n + 1).f_code.co_name
    if debug: 
        print timestamp(), currentFuncName(1), object 
        sys.stdout.flush() 

def logo(extra = ''): 
    currentFuncName = lambda n=0: sys._getframe(n + 1).f_code.co_name
    if constants.debug: 
        print timestamp(), currentFuncName(1) + ' open ' + extra
        sys.stdout.flush() 

def logc(extra = ''): 
    currentFuncName = lambda n=0: sys._getframe(n + 1).f_code.co_name
    if constants.debug: 
        print timestamp(), currentFuncName(1) + ' close ' + extra
        sys.stdout.flush() 

def log_nv(name, value): 
    currentFuncName = lambda n=0: sys._getframe(n + 1).f_code.co_name
    if constants.debug: 
        print timestamp(), currentFuncName(1), name + ' = ' + str(value) 
        sys.stdout.flush() 

def pause():
    sys.stdout.flush() 
    raw_input('')
    
def parse_sys_argv(sys_argv, key_list): 

    # Check command line argument pairs is of the form "./command -key value" and returns it as a dictionary. 
    # Use "if if ..." to process multiple key value pairs in sequence 
    # Use "if elif ..." to process only one key value pair 
    # No dependency check among argument pairs 
    
    args = {} 
    if len(sys_argv) % 2 == 0:
        raise ValueError('Unexpected arguments')
    else: 
        for i in range(1, len(sys_argv)): 
            if i % 2 == 1:
                if not sys_argv[i].startswith('-'):
                    raise ValueError(sys_argv[i] + ' should begin with -')
                if sys_argv[i][1:] not in key_list:
                    raise ValueError(sys_argv[i] + ' is not a recognized key') 
            else:
                args[sys_argv[i - 1][1:]] = sys_argv[i]
    log(args)
    return args 
    
if __name__ == "__main__":

    print timestamp() 
    print parse_sys_argv(sys.argv, ['s', 'f', 'o', 'gg'])
    
    