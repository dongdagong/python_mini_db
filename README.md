# Python implementation of SQL-like database queries 

Queries support the following: 

- Column selection
- Row filtering
- Result set ordering 
- Grouping and aggregation functions 

Code is tested in Python 2.7.14 

# Test queries  

To import sample records as defined in the original task requirement email: 

./database.py -import input001.txt 

To run the sample queries: 

./query -s PROJECT,SHOT,VERSION,STATUS -o FINISH_DATE,INTERNAL_BID

./query -s PROJECT,SHOT,VERSION,STATUS -f FINISH_DATE=2006-07-22

./query -s PROJECT,INTERNAL_BID:sum,SHOT:collect -g PROJECT

./query -s PROJECT,INTERNAL_BID -f 'PROJECT="the hobbit" OR PROJECT="lotr"'


To regenerate the database using another input file that has a few more records: 

./database.py -import input002.txt 

A few more queries on input002: 

./query 

./query -s PROJECT,SHOT,INTERNAL_BID -f 'PROJECT="lotr" OR INTERNAL_BID>22 AND INTERNAL_BID<=115.00' -o PROJECT,INTERNAL_BID 

./query -s PROJECT,SHOT,VERSION:collect,INTERNAL_BID:sum -g PROJECT,SHOT


To run a list of more test cases (output makes more sense if imported from input002.txt): 

./query002 

To generate 300 sample records and refresh the db: 

./inputgen.py 300 input300.txt 

./database.py -import input300.txt 


# List of files 

database.dat - Imported db records stored in this file 

database.py - Main script 

expression.py - Translate logical expressions from infix to postfix 

inputgen.py - Generate random test data 

input001.txt - Input records as defined in the original requirement email 

input002.txt - Has a bit more than input001.txt, records in database.dat is at the moment from this file 

query - Command line interface to main methods defined in database.py 

query002 - Predefined test cases. Executable as ./query002 

query002b - SQL queries mapped from test cases in query002 


