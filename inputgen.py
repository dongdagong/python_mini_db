#!/usr/bin/env python 

from random import randint
import time 
import sys

# Generate a large file with random values to use as a input file to ./database.py 
    
def get_finish_date():
    return time.strftime('%Y-%m-%d', time.localtime(randint(876645812, 1760258612)))

def get_created_date():
    return time.strftime('%Y-%m-%d %H:%M', time.localtime(randint(876645812, 1760258612)))
    
if __name__ == "__main__":

    num_rows = int(sys.argv[1]) # number of records to be generated 
    
    with open(sys.argv[2], 'w') as file: 
    
        file.write('PROJECT|SHOT|VERSION|STATUS|FINISH_DATE|INTERNAL_BID|CREATED_DATE\n')
        
        for i in range(num_rows): 
            file.write( \
                'proj' + str(randint(1, 100)).rjust(5, '0')  + '|' + \
                str(randint(800, 900)) + '|' + \
                str(randint(1, 100)) + '|' + \
                'status' + str(randint(1, 5)).rjust(3, '0')  + '|' + \
                get_finish_date()  + '|' + \
                str(randint(1, 100)) + '.00|' + \
                get_created_date()  + '\n' 
            ) 
            
    print(str(num_rows) + ' records written to ' + sys.argv[1]) 
    
